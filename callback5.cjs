/* 
    Problem 5: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/

const getThanosBoard = require("./callback1.cjs")
const getThanosBoardList = require("./callback2.cjs")
const getListCards = require("./callback3.cjs")

function problem5() {
    setTimeout(() => {

        let thanosID = "mcu453ed";
        getThanosBoard(thanosID, function (err, data) {
            if (err) {
                console.error(err)
            } else {
                console.log(data)

                getThanosBoardList(thanosID, function (err, data) {
                    if (err) {
                        console.error(err)
                    } else {
                        console.log(data)

                        let mindListID = "qwsa221"
                        getListCards(mindListID, function (err, data) {
                            if (err) {
                                console.error(err)
                            } else {
                                console.log(data)

                                let spaceListID = "jwkh245"
                                getListCards(spaceListID, function (err, data) {
                                    if (err) {
                                        console.error(err)
                                    } else {
                                        console.log(data)
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })
    }, 2 * 1000);
}

module.exports = problem5;