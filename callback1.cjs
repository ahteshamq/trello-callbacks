/* 
    Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.
*/

const boards = require("./boards.json")




function problem1(boardID, callback) {
    if (boardID === undefined || typeof boardID !== 'string') {
        callback("Invalid board id.")
    } else {
        setTimeout(() => {
            let boardDetail = boards.filter(board => {
                if (board.id === boardID) {
                    return board
                }
            })
            if (boardDetail.length === 0) {
                callback("Board ID does not exist")
            } else {
                callback(null, boardDetail[0])
            }
        }, 2 * 1000);
    }

}

module.exports = problem1;
