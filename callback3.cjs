/* 
    Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.
*/
const cards = require("./cards.json")



function problem3(listID, callback) {
    if (listID === undefined || typeof listID !== 'string' || typeof callback !== 'function') {
        if (typeof callback === 'function') {
            callback("Invalid list ID passed.")
        } else {
            console.error("Invalid arguments passed.")
        }
    } else {
        setTimeout(() => {
            if (cards.hasOwnProperty(listID)) {
                callback(null, cards[listID])
            } else {
                callback("List ID does not exist.")
            }
        }, 2 * 1000);
    }
}

module.exports = problem3;

