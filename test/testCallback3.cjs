const problem3 = require("../callback3.cjs")

function callback(err, data) {
    if (err) {
        console.error(err)
    } else {
        console.log(data)
    }
}

problem3("", callback)