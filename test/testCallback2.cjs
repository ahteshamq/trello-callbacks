const problem2 = require("../callback2.cjs")

function callback(err, data) {
    if (err) {
        console.error(err)
    } else {
        console.log(data)
    }
}

problem2("abc122dc", callback)