/* 
    Problem 6: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/
const lists = require("./lists.json")

const getThanosBoard = require("./callback1.cjs")
const getThanosBoardList = require("./callback2.cjs")
const getListCards = require("./callback3.cjs")

function problem6() {
    setTimeout(() => {

        let thanosID = "mcu453ed";
        console.log("Thanos board information.")
        getThanosBoard(thanosID, function (err, data) {
            if (err) {
                console.error(err)
            } else {
                console.log(data)

                console.log("All the lists for thanos board.")
                getThanosBoardList(thanosID, function (err, data) {
                    if (err) {
                        console.error(err)
                    } else {
                        console.log(data)

                        console.log("All cards for all lists")
                        let listArr = Object.entries(lists)
                        listArr.forEach(cardData => {
                            // [1] contains list array of a boardID's
                            cardData[1].forEach(card => {
                                getListCards(card.id, function (err, data) {
                                    if (err) {
                                        console.error(err)
                                    } else {
                                        console.log(data)
                                    }
                                })
                            })
                        })
                    }
                })
            }
        })
    }, 2 * 1000);
}

module.exports = problem6;
