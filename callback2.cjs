/* 
    Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.
*/

const boards = require("./boards.json")
const lists = require("./lists.json")

function problem2(boardID, callback) {
    if (boardID === undefined || typeof boardID !== 'string') {
        callback("Invalid board id.")
    } else {
        setTimeout(() => {
            let boardDetail = boards.filter(board => {
                if (board.id === boardID) {
                    return board.id
                }
            })
            if (boardDetail.length === 0) {
                callback("Board ID does not exist")
            } else {
                if (lists.hasOwnProperty(boardID)) {
                    callback(null, lists[boardID])
                } else {
                    callback("Board ID doesn not exist in list.")
                }
            }
        }, 2 * 1000);
    }
}

module.exports = problem2;